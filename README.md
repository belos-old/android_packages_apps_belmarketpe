# BelMarket Privileged Extension

This enables BelMarket to install and delete apps without needing "Unknown Sources" to be enabled (e.g. just like Google Play does).  It also enables BelMarket to install updates in the background without the user having to click "install".

When BelMarket is installed as a normal Android app, installing, updating, and removing apps can only be done by sending requests to Android operating system.  BelMarket cannot execute these operations itself.  Android shows a screen on every install/update/delete to confirm this is what the user actually wants.  This is a security feature of Android to prevent apps or websites from installing malware without user intervention.

BelMarket Privileged Extension has elevated permissions which allow it to do installs and deletes.  It gives only BelMarket access to its install and delete commands.  In order for BelMarket Privileged Extension to get these "privileged" powers, it must be installed as part of your system by either being flashed as an _update.zip_ or by being built into an Android device or ROM. On Android 4 and older, it can be installed directly if you have root on your device.


## Design

BelMarket Privileged Extension is designed on the principals of "least privilege", so that elevated powers are only granted where they are absolutely needed, and those powers are limited as much as possible.  Therefore, the code that runs with increased powers is very small and easy to audit.  This is in contrast to how typical built-in app stores are granted all of the privileges available to a "system priv-app". 

Advantages of this design:

* "Unknown Sources" can remain disabled
* Can easily be built into devices and ROMs
* Reduced disk usage in the system partition
* System updates don't remove BelMarket


## How do I install it on my device?

The best way to install BelMarket Privileged Extension is to flash the _update.zip_ file using the standard mechanism for flashing updates to the ROM. This requires the device have an unlocked bootloader. A custom Recovery firmware is recommended. This is the same procedure as flashing "gapps" after flashing a ROM onto your device.

Installing the BelMarket Privileged Extension directly from the BelMarket app requires root access and is only possible on Android versions older than 5.0. It is not possible on Android 5.1, 6.0, and newer. To install the extension open the settings inside the BelMarket app, enable "Expert mode" and then enable "Privileged Extension". It will lead you to the extension app which will guide you through the installation process.

There are potential risks to rooting and unlocking your device, including:

* often requires using random, unverifed software
* bootloader unlock often voids warranty
* official updates might stop working with unlocked bootloader
* other functionality may break, like Android Pay, DRM-protected content playing, camera enhancements, etc.


## How do I build it into my ROM?

BelMarket Privileged Extension is designed to be built into ROMs and signed by the ROM key.  BelMarket only gets permissions via BelMarket Privileged Extension's internal key check, not via having a matching signing key or via `"signature" protectionLevel`.  This git repo includes an _Android.mk_ so it can be directly included via `repo`.   Add `FDroidPrivilegedExtension` to the `PRODUCT_PACKAGES` list to include it in the system image, and use a `repo` manifest like this:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest>

  <remote name="belos" fetch="https://gitlab.com/belos/" />
  <project path="packages/apps/FDroidPrivilegedExtension"
           name="android_packages_apps_belmarketpe.git" remote="belos"
           revision="refs/tags/0.2-belos" />

</manifest>
```

By default, BelMarket Privileged Extension trusts only the official BelMarket builds, and we recommend that https://belmarket.ir/BelMarket.apk is also included in the ROM. You can verify the binaries using both the APK signature and the PGP key: https://f-droid.org/FDroid.apk.asc

APK signing certificate SHA-256 fingerprint:
```
c8a2e9bccf597c2fb6dc66bee293fc13f2fc47ec77bc6b2b0d52c11f51192ab8
```

PGP signing key fingerprint is:
```

```

There is more documentation on this here:
https://f-droid.org/wiki/page/Release_Channels_and_Signing_Keys


## Direct download

You can [download the extension from our repo](https://shop.belmarket.ir/org.belos.belmarket.privileged.apk). #TODO change url

## Building with Gradle

Build a complete "update.zip" to flash to a device to install BelMarket and the Privileged Extension:

    ./gradlew assembleUpdateZipFromBinariesDebug

Build an "update.zip" to flash to a device to install just the Privileged Extension:

    ./gradlew assembleUpdateZipDebug

Build the standlone APK using:

    ./gradlew assembleRelease

In order to have final, signed release versions that are ready for installing, a release signing key must be set up in _signing.properties_ with these contents:

    key.store=/path/to/release-keystore.jks
    key.store.password=mysecurestorepw
    key.alias=release
    key.alias.password=mysecurekeypw

## Testing in the Emulator

To test the priveleged extension in the emulator, one has to modify the system.img
It is located under the Android SDK install path:
```
$ANDROID_HOME/system-images/android-23/google_apis/x86_64
```
for an API 23 (Marshmallow, 6.0) x86_64 image with Google APIs.

To install it, first build the standalone APK, and then run these in the above directory

    mkdir system
    mount -o loop system.img system
    mkdir system/priv-app/FDroidPrivilegedExtension

Copy the standalone APK to the above created 'FDroidPrivilegedExtension' folder
After that, you need to set the correct SELinux context using:

    chcon -R --reference=app/webview system/priv-app/FDroidPrivilegedExtension

Upon booting the emulator it should have the PrivExt installed, and one can also install the F-Droid app this way, or via the normal methods.
